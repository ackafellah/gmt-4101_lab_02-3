package exercice_4;
/*
 * Expliquer pourquoi le code ne compile pas ?
 * 
 * ERREUR 1 : Les m�thodes d'une interface ne peuvent pas �tre de type "private" car elles doivent �tre red�finies (et donc accessibles depuis les classes qui impl�mentent l'interface).
 * --> on remplace "private" par "protected" ou "public".
 * 
 * ERREUR 2 : La classe Chose doit red�finir TOUTES les m�thodes abstraites de l'interface.
 * --> Il faut donc r�ecrire la m�thode C.
 * 
 * ERREUR 3 : Il n'existe aucune instance de la classe Chose nomm�e "c".
 * --> On remplace l'instance "b" par "c".
 */

interface Truc {
	public int methodeA();
	public void methodeB();
	public int methodeC();
} 

class Chose implements Truc {
	public int methodeA() {
		return 2;
	}
	public void methodeB() {
		System.out.println(methodeA());
	}
	public int methodeC() {
		return 3;
	}
} 

class Exercice4 {
	public static void main(String[] arg) {
		Chose c = new Chose();
		c.methodeB();
	}
}