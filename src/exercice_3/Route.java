package exercice_3;

public class Route {
	private String nom;
	private Ville start;
	private Ville end;
	
	/**
	 * Constructeur d'une route bidirectionnelle
	 * 
	 * @author Boffy William
	 * @param Ville v1, ville de d�part
	 * @param Ville v2, ville d'arriv�e
	 * @param String n, nom de la route
	 * 
	 */
	Route(String n, Ville v1, Ville v2){
		this.nom = n;
		this.start = v1;
		this.end = v2;
		this.start.update(this.getEnd());
		//indications();
	}
	
	/**
	 * Affiche des indications sur la route
	 * 
	 * @author Boffy William
	 */
	public void indications() {
		System.out.println("\nAjout de la route '" + this.nom + "', reliant les villes de '" + this.getStart() + "' et '" + this.getEnd() + "'.");
		System.out.println("Depuis " + this.getStart() + ", on peut joindre : " + this.getStart().getVillesJoignables());
		System.out.println("Depuis " + this.getEnd() + ", on peut joindre : " + this.getStart().getVillesJoignables() + "\n");
	}
	
	/**
	 * Accesseur sur l'attribut nom
	 * 
	 * @author Boffy William
	 * @return nom
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * Retourne la ville de d�part
	 * 
	 * @author Boffy William
	 * @return Ville, start
	 */
	public Ville getStart() {
		return this.start;
	}
	
	/**
	 * Retourne la ville d'arriv�e
	 * 
	 * @author Boffy William
	 * @return Ville, end
	 */
	public Ville getEnd() {
		return this.end;
	}
}
