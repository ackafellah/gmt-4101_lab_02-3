package exercice_1_2;
public class TestListe {
	
	/**
	 * M�thode de test de la classe Liste
	 * 
	 * @author Nom Pr�nom
	 * @param  nom description
	 */
	public static void main (String[] args){
		//TEST EXERCICE 01
		System.out.println("EXERCICE 01\nTEST LISTE 1 :");
		Liste l = new Liste("poulet");
		System.out.println("l : " + l);
		l = l.ajouterEnTete("dindon");
		l = l.ajouterEnTete("dinde");
		l = l.ajouterEnTete("ch�vre");
		System.out.println("l : " + l);
		System.out.println("longueur de l : " + l.longueur());
		
		System.out.println("\nTEST LISTE 2 :");
		
		Liste l2 = new Liste();
		System.out.println("l2 : " + l2);
		System.out.println("L2 vide ? " + l2.estVide());
		l2 = l2.ajouterEnTete("fleur");
		System.out.println("l2 : " + l2);
		System.out.println("L2 vide ? " + l2.estVide());
		l2 = l2.ajouterEnTete("arbre");
		System.out.println("l2 : " + l2);
		System.out.println("longueur de l2 : " + l2.longueur());
		
		//TEST EXERCICE 02
		System.out.println("\nEXERCICE 02");
		System.out.println("l : " + l);
		System.out.println("l contient poulet ? " + l.contient("poulet"));
		System.out.println("l2 : " + l2);
		System.out.println("l2 contient poulet ? " + l2.contient("poulet"));

		System.out.println("\nl : " + l);
		l = l.ajouterEnQueue("queue");
		System.out.println("l : " + l);

		System.out.println("\nl : " + l);
		System.out.println("l2 : " + l2);
		l.concat(l2);
		System.out.println("l2  (soit l + l2): " + l2);
		
		Liste l3 = l.inverse();
		System.out.println("\nl : " + l);
		System.out.println("l3 : " + l3);

		System.out.println("\nl3 (avant copie) : " + l3);
		Liste l4 = l3.copie();
		System.out.println("l3 (apr�s copie) : " + l3);
		System.out.println("l4 : " + l4);

		System.out.println("l3 == l4 ? " + (l3==l4));
		System.out.println("l3.equals(l4) ? " + (l3.equals(l4)) + " (c'est normal, non red�fini...)");
		
		Liste l5 = l4.ajouterEnQueue("DIFFERENCE");
		System.out.println("\nl4 : " + l4);
		System.out.println("l5 : " + l5);
	}
}
